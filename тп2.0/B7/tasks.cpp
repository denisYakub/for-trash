#include <iterator>
#include <iostream>
#include <cmath>
#include <vector>
#include <iostream>
#include <list>
#include <memory>
#include <string>
#include <map>
#include <algorithm>
#include <functional>
#include <sstream>

#include "shape.h"
#include "triangle.h"
#include "square.h"
#include "circle.h"
#include"addition.h"

void task1()
{
  std::vector<double> vector{ std::istream_iterator<double>(std::cin), std::istream_iterator<double>() };
  if ((!std::cin.eof())) 
  {
    throw std::invalid_argument("<Invalid input>");
  }

  std::for_each(vector.begin(), vector.end(), PImultiplier());

  std::copy(vector.begin(), vector.end(), std::ostream_iterator<double>(std::cout, " "));
}

void task2()
{
  std::list<std::shared_ptr<Shape>> shapes;
  std::string input;
  while (std::getline(std::cin >> std::ws, input))
  {
    std::stringstream stream(input);
    std::string temp;
    std::string shape;
    int x, y;
    std::getline(stream, temp);
    if ((temp.find('(') == std::string::npos) || (temp.find(';') == std::string::npos) || (temp.find(')') == std::string::npos))
    {
      throw std::invalid_argument("<INVALID SEPARATOR>");
    }
    shape = temp.substr(0, temp.find('('));
    shape.erase(std::remove_if(shape.begin(), shape.end(), isspace), shape.end());
    temp = temp.substr(temp.find('(') + 1);
    x = stoi(temp.substr(0, temp.find(';')));
    y = stoi(temp.substr(temp.find(';') + 1, temp.length() - 1));
    if (shape == "CIRCLE")
    {
      shapes.push_back(std::make_shared<Circle>(x, y));
    }
    else if (shape == "SQUARE")
    {
      shapes.push_back(std::make_shared<Square>(x, y));
    }
    else if (shape == "TRIANGLE")
    {
      shapes.push_back(std::make_shared<Triangle>(x, y));
    }
    else
    {
      throw std::invalid_argument("<INVALID SHAPE>");
    }
  }

  std::cout << "Original:\n";
  std::for_each(shapes.begin(), shapes.end(), print);

  shapes.sort(compLeftRight);

  std::cout << "Left-Right:\n";
  std::for_each(shapes.begin(), shapes.end(), print);

  shapes.sort(std::bind(compLeftRight, std::placeholders::_2, std::placeholders::_1));

  std::cout << "Right-Left:\n";
  std::for_each(shapes.begin(), shapes.end(), print);

  shapes.sort(compUpper);

  std::cout << "Top-Bottom:\n";
  std::for_each(shapes.begin(), shapes.end(), print);

  shapes.sort(std::bind(compUpper, std::placeholders::_2, std::placeholders::_1));

  std::cout << "Bottom-Top:\n";
  std::for_each(shapes.begin(), shapes.end(), print);
}
