#include <iostream>
#include <iterator>
#include <algorithm>

#include "functor.h"

int main()
{
  try {
    Functor fun = std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), Functor());

    if (!std::cin.eof() && std::cin.fail())
    {
        throw std::invalid_argument("<Incorrect input>");
        return -1;
    }
    else
    {
        fun.printData();
        return 0;
    }
  }
  catch(const std::invalid_argument &err)
  {
    std::cerr << err.what() << std::endl;
    return -1;
  }
  catch (const std::exception &err)
  {
    std::cerr << err.what() << std::endl;
    return -1;
  }
}

