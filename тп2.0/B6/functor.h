#ifndef B6_FUNCTOR_H
#define B6_FUNCTOR_H

class Functor
{
public:
    void operator ()(const int data);
    void printData();

private:
    int max;
    int min;
    int pos;
    int neg;
    int first;
    int last;
    int amountOfElem;
    long long int oddSum;
    long long int evenSum;
};

#endif //B6_FUNCTOR_H
