#include <iostream>
#include <exception>

void task1();
void task2();

int main(int argc, char* argm[])
{
  try
  {
    if (argc == 2)
    {
      switch (std::stoi(argm[1]))
      {
      case 1:
          task1();
          break;
      case 2:
          task2();
          break;
      default:
          throw std::invalid_argument("<Invalid argument given>");
          return -1;
      }
    }
    else
    {
      throw std::invalid_argument("<Invalid number of arguments>");
      return -1;
    }
  }
  catch (const std::invalid_argument& err)
  {
    std::cerr << err.what() << std::endl;
    return -1;
  }
  catch (const std::exception& err)
  {
    std::cerr << err.what() << std::endl;
    return -1;
  }
  return 0;
}

