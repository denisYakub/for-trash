#include <iostream>
#include <string>
#include <exception>
void task(const size_t width = 40);

int main(int args, char* argm[])
{
  try
  {
    size_t width = 40;

    if (args > 1)
    {
      if (args != 3)
      {
        throw std::invalid_argument("<INVALID ARGUMENT>");
      }
      else if (std::string(argm[1]) != "--line-width")
      {
        throw std::invalid_argument("<INVALID ARGUMENT>");
      }
      else
      {
        width = std::stoi(argm[2]);

        if (width < 25)
        {
          throw std::invalid_argument("<INVALID WIDTH>");
        }
      }
    }
    task(width);
  }
  catch (std::invalid_argument& err)
  {
    std::cerr << err.what() << std::endl;
    return -1;
  }
  catch (std::exception& err)
  {
    std::cerr << err.what() << std::endl;
    return -1;
  }
  return 0;
}
