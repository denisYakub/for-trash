#ifndef CIRCLE_H
#define CIRCLE_H

#include "shape.h"

class Circle : public Shape
{
public:
    Circle(const int x, const int y);

    void draw() const override;
};

#endif // !CIRCLE_H
