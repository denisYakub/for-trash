#include <iostream>
#include <sstream>
#include <algorithm>
#include <string>

#include "PhoneBook.hpp"
#include "factorial.hpp"

void add(std::stringstream& sstream, PhoneBook& phoneBook);
void store(std::stringstream& sstream, PhoneBook& phoneBook);
void insert(std::stringstream& sstream, PhoneBook& phoneBook);
void deleteMark(std::stringstream& sstream, PhoneBook& phoneBook);
void show(std::stringstream& sstream, PhoneBook& phoneBook);
void move(std::stringstream& sstream, PhoneBook& phoneBook);

void task1()
{
  PhoneBook phoneBook;
  std::string line;

  while (std::getline(std::cin, line))
  {
    std::stringstream sstream(line);
    sstream >> line;
    if (line == "add")
    {
      add(sstream, phoneBook);
    }
    else if (line == "store")
    {
      store(sstream, phoneBook);
    }
    else if (line == "insert")
    {
      insert(sstream, phoneBook);
    }
    else if (line == "delete")
    {
      deleteMark(sstream, phoneBook);
    }
    else if (line == "show")
    {
      show(sstream, phoneBook);
    }
    else if (line == "move")
    {
      move(sstream, phoneBook);
    }
    else
    {
      std::cout << "<INVALID COMMAND>" << std::endl;
    }
  }
}

void task2()
{
  Container factorial;
  std::copy(factorial.begin(), factorial.end(), std::ostream_iterator<size_t>(std::cout, " "));
  std::cout << std::endl;
  std::reverse_copy(factorial.begin(), factorial.end(), std::ostream_iterator<size_t>(std::cout, " "));
  std::cout << std::endl;
}
