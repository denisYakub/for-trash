#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "shape.h"

class Triangle : public Shape
{
public:
    Triangle(const int x, const int y);

    void draw() const override;
};

#endif // !TRIANGLE_H
