#include <iterator>

class Container
{
public:
    class Iterator;
    Iterator begin();
    Iterator end();
};

class Container::Iterator :
        public std::iterator<std::bidirectional_iterator_tag, size_t, size_t, size_t*, size_t>
{
public:
    Iterator();

    const size_t & operator *();
    const size_t * operator ->();

    bool operator == (const Iterator& rhs) const;
    bool operator != (const Iterator& rhs) const;

    Iterator & operator ++();
    Iterator& operator --();
    Iterator operator ++(int);
    Iterator operator --(int);

private:
    size_t n;
    size_t factor;
};
