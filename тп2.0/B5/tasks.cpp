#include <iostream>
#include <set>
#include <sstream>
#include <algorithm>
#include <list>
#include<vector>
#include "string"

#include "Point.hpp"

std::set<std::string> getWords()
{
  std::string line;
  std::set<std::string> words;

  while (std::getline(std::cin, line))
  {
    std::istringstream inPutLine(line);
    std::string word;
    while (inPutLine >> word)
    {
      words.insert(word);
    }
  }
  return words;
}

void readShape(std::vector<Point>& shape, std::stringstream& input)
{
  size_t count;
  Point point;
  char tempCharacter;

  input >> std::ws >> count;
  while ((input >> std::ws >> tempCharacter))
  {
    if (tempCharacter != '(')
    {
      throw std::invalid_argument("<Invalid input:(>");
    }

    input >> std::ws >> point.x;
    input >> std::ws >> tempCharacter;
    if (tempCharacter != ';')
    {
      throw std::invalid_argument("<Invalid input:;>");
    }

    input >> std::ws >> point.y;
    input >> std::ws >> tempCharacter;
    if (tempCharacter != ')')
    {
      throw std::invalid_argument("<Invalid input:)>");
    }

    shape.push_back({ point.x, point.y });
  }

  if (count != shape.size())
  {
    throw std::invalid_argument("<Invalid number of points provided>");
  }
}

bool isSquare(const std::vector<Point>& shape)
{
  if (shape.size() != 4)
  {
    return false;
  }

  if (shape[1].x != shape[0].x)
  {
    if (std::abs(shape[1].x - shape[0].x) != std::abs(shape[2].y - shape[1].y))
    {
      return false;
    }
  }
  else
  {
    if (std::abs(shape[2].x - shape[1].x) != std::abs(shape[1].y - shape[0].y))
    {
      return false;
    }
  }

  return true;
}

bool compareShape(const std::vector<Point>& shape1, const std::vector<Point>& shape2)
{
  if (shape1.size() < shape2.size())
  {
    return true;
  }

  if (shape1.size() == 4 && shape2.size() == 4)
  {
    if (isSquare(shape1))
    {
      if (isSquare(shape2))
      {
        return false;
      }
      else
      {
        return true;
      }
    }
  }

  return false;
}

void printShape(const std::vector<Point>& shape)
{
  std::cout << shape.size() << "  ";
  for (auto point : shape)
  {
    std::cout << "(" << point.x << ";" << point.y << ")" << " ";
  }
  std::cout << std::endl;
}

void task1()
{
  std::set<std::string> words = getWords();
  for (const std::string &words: words)
  {
    std::cout << words << std::endl;
  }
}

void task2()
{
  std::list<std::vector<Point>> shapes;
  size_t verticesCounter[3]{0, 0, 0};

  std::vector<Point> vector;
  std::string input;

  while (std::getline(std::cin >> std::ws, input))
  {
    std::stringstream inputStream(input);
    readShape(vector, inputStream);
    if (!vector.empty())
    {
      shapes.push_back(vector);
    } 
    /*else 
    {
      throw std::invalid_argument("<Invalid input>");
    }*/
    vector.clear();
  }

  size_t vertices = 0;
  for (std::vector<Point> shape : shapes)
  {
    vertices += shape.size();
  }

  for (std::vector<Point> shape : shapes)
  {
    if (shape.size() == 3)
    {
      ++verticesCounter[0];
    }
    else if (shape.size() == 4)
    {
      if (isSquare(shape))
      {
        ++verticesCounter[1];
      }
      ++verticesCounter[2];
    }
  }

  for (auto shape = shapes.begin(); shape != shapes.end();)
  {
    if (shape->size() == 5)
    {
      shape = shapes.erase(shape);
    }
    else
    {
      ++shape;
    }
  }

  std::vector<Point> points;
  for (std::vector<Point> shape : shapes)
  {
    points.push_back(shape[0]);
  }

  shapes.sort(compareShape);

  std::cout << "Vertices: " << vertices << std::endl
            << "Triangles: " << verticesCounter[0] << std::endl
            << "Squares: " << verticesCounter[1] << std::endl
            << "Rectangles: " << verticesCounter[2] << std::endl;

  std::cout << "Points: ";
  for (Point point : points)
  {
    std::cout << "(" << point.x << ";" << point.y << ")" << " ";
  }
  std::cout << std::endl;

  std::cout << "Shapes:" << std::endl;
  for (std::vector<Point> shape : shapes)
  {
    printShape(shape);
  }
}
