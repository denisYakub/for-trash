#ifndef SHAPE_H
#define SHAPE_H

#include <memory>

class Shape
{
public:
    Shape(const int x, const int y);
    virtual ~Shape() = default;

    bool isToTheLeft(const std::shared_ptr<Shape> & shape) const;
    bool isUpper(const std::shared_ptr<Shape> & shape) const;

    virtual void draw() const = 0;

protected:
    int x_;
    int y_;
};

#endif // !SHAPE_H
