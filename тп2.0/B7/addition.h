#include <iostream>
#include <list>
#include <memory>
#include <string>
#include <map>
#include <algorithm>
#include <functional>
#include <sstream>
#include <cmath>

#include "shape.h"
#include "triangle.h"
#include "square.h"
#include "circle.h"

struct PImultiplier
{
public:
    void operator() (double& data)
    {
        data *= M_PI;
    }
};

void print(const std::shared_ptr<Shape> & shape)
{
    shape->draw();
}

bool compLeftRight(const std::shared_ptr<Shape> & lhs, const std::shared_ptr<Shape> & rhs)
{
    return lhs->isToTheLeft(rhs);
}

bool compUpper(const std::shared_ptr<Shape> & lhs, const std::shared_ptr<Shape> & rhs)
{
    return lhs->isUpper(rhs);
}
