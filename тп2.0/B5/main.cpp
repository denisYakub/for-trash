#include <iostream>

void task1();
void task2();

int main(int argc, char *argm[])
{
  try
  {
    if (argc == 2)
    {
      switch (std::atoi(argm[1])) {
          case 1:
              task1();
              break;
          case 2:
              task2();
              break;
          default:
              throw std::invalid_argument("Invalid task number");
              return -1;
      }
      return 0;
    }
    else
    {
      throw std::invalid_argument("Invalid number of input parameters");
      return -1;
    }
  }
  catch (const std::invalid_argument &err)
  {
    std::cerr << err.what() << '\n';
    return -1;
  }
  catch (const std::exception &err)
  {
    std::cerr << err.what() << '\n';
    return -1;
  }
}
