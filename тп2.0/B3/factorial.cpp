#include "factorial.hpp"

Container::Iterator Container::begin()
{
  return Container::Iterator{};
}

Container::Iterator Container::end()
{
  Container::Iterator iter{};
  std::advance(iter, 10);
  return iter;
}

Container::Iterator::Iterator() : n(1), factor(1)
{}

const size_t &Container::Iterator::operator *()
{
  return factor;
}

const size_t *Container::Iterator::operator ->()
{
  return &factor;
}

Container::Iterator & Container::Iterator::operator ++()
{
  factor *= ++n;
  return *this;
}

Container::Iterator Container::Iterator::operator ++(int)
{
  Container::Iterator nextIter = *this;
  factor *= ++n;
  return nextIter;
}

Container::Iterator & Container::Iterator::operator --()
{
  factor /= n--;
  return *this;
}

Container::Iterator Container::Iterator::operator --(int)
{
  Container::Iterator prevIter = *this;
  factor /= n--;
  return prevIter;
}

bool Container::Iterator::operator == (const Container::Iterator &rhs) const
{
  return n == rhs.n;
}

bool Container::Iterator::operator != (const Container::Iterator &rhs) const
{
  return n != rhs.n;
}
