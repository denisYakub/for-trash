#ifndef SQUARE_H
#define SQUARE_H

#include "shape.h"

class Square : public Shape
{
public:
    Square(const int x, const int y);

    void draw() const override;
};

#endif // !SQUARE_H
