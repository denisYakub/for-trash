#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>

#include "DataStruct.h"

int getKey(const std::string &str)
{
  if (str.empty())
  {
    throw std::invalid_argument("<Invalid input, key missing>");
  }

  int key = std::stoi(str);

  if (key > 5 || key < -5)
  {
    throw std::logic_error("<Input key out of range [-5, 5]>");
  }

  return key;
}

void print(const std::vector<DataStruct> &array)
{
  for (auto i = array.begin(); i != array.end(); ++i)
  {
    std::cout << i->key1 << ", " << i->key2 << ", " << i->str << std::endl;
  }
}

bool compare(const DataStruct &data1, const DataStruct &data2)
{
  if (data1.key1 != data2.key1)
  {
    return (data1.key1 < data2.key1);
  }
  else if (data1.key2 != data2.key2)
  {
    return (data1.key2 < data2.key2);
  }
  else
  {
    return (data1.str.size() < data2.str.size());
  }
}

void task()
{
  std::vector<DataStruct> vector;

  std::string line;
  while (std::getline(std::cin, line))
  {
    std::stringstream stream(line);

    std::string command;
    stream >> command;

    std::stringstream input(line);
    std::string sym;

    std::getline(input >> std::ws, sym, ',');
    int key1 = getKey(sym);
    sym.clear();

    std::getline(input >> std::ws, sym, ',');
    int key2 = getKey(sym);
    sym.clear();

    std::getline(input >> std::ws, sym, '\n');

    if (sym.empty())
    {
      throw std::invalid_argument("invalid input, missing string");
    }

    vector.push_back({key1, key2, sym});
  }

  std::sort(vector.begin(), vector.end(), compare);
  print(vector);
}

