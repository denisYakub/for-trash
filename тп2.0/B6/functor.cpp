#include <iostream>
#include <algorithm>
#include <iomanip>

#include "functor.h"

void Functor::operator() (const int data)
{
  if (data > 0)
  {
    ++pos;
  }
  else if (data < 0)
  {
    ++neg;
  }

  if (data % 2 == 0)
  {
    evenSum += data;
  }
  else
  {
    oddSum += data;
  }

  if (amountOfElem == 0)
  {
    first = data;
    max = data;
    min = data;
  }

  max = std::max(max, data);
  min = std::min(min, data);
  last = data;
  ++amountOfElem;
}

void Functor::printData()
{
  if (amountOfElem == 0)
  {
    std::cout << "No Data" << "\n";
    return;
  }

  auto mean = double((evenSum + oddSum)) / amountOfElem;

  std::cout << "Max: " << max << "\n";
  std::cout << "Min: " << min << "\n";
  std::cout << "Mean: " << std::setprecision(2) << std::fixed << mean << "\n";
  std::cout << "Positive: " << pos << "\n";
  std::cout << "Negative: " << neg << "\n";
  std::cout << "Odd Sum: " << oddSum << "\n";
  std::cout << "Even Sum: " << evenSum << "\n";
  std::cout << "First/Last Equal: " << (first == last ? "yes" : "no") << "\n";
}
