#include <iostream>

#include "triangle.h"

Triangle::Triangle(const int x, const int y):Shape(x, y)
{
}

void Triangle::draw() const
{
  std::cout << "TRIANGLE (" << x_ << ';' << y_ << ")\n";
}
